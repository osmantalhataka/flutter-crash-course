import 'package:flutter/material.dart';
import '../models/location.dart';

class LocationDetail extends StatelessWidget {
  final Location location;
  const LocationDetail(this.location);

  List<Widget> _renderFacts(Location location) {
    var x = <Widget>[];
    for (int i = 0; i < location.facts.length; i++) {
      x.add(Container(
        padding: const EdgeInsets.fromLTRB(20, 20, 20, 10),
          child: Text(location.facts[i].title,
              textAlign: TextAlign.left,
              style: const TextStyle(fontSize: 24.0, color: Colors.black))
      ));
      x.add(Container(
        padding: const EdgeInsets.fromLTRB(20, 20, 20, 10),
        child: Text(location.facts[i].text),
      ));
    }
    return x;
  }

  Widget _bannerImage(String url, double height) {
    return Container(
        constraints: BoxConstraints.tightFor(height: height),
        child: Image.network(url, fit: BoxFit.fitWidth));
  }

  List<Widget> _renderBody(BuildContext context, Location location) {
    var x = <Widget>[];
    x.add(_bannerImage(location.url, 200.0));
    x.addAll(_renderFacts(location));
    return x;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(location.name),
        ),
        body: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: _renderBody(context, location)));
  }
}
