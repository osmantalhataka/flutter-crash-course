import '../models/location_fact.dart';
import '../models/location.dart';

class MockLocation{
  static Location FetchAny =
    Location(
        "Forest in Japan",
        "https://windows10spotlight.com/wp-content/uploads/2017/10/496677f5718fa1b8e0fdc225ece7d111.jpg",
        <LocationFact> [
          LocationFact("Summary", "Here are some facts about this cool forest in Japan."),
          LocationFact("Transportation", "Kyoto Airport is relatively close to the forest."),
        ]
    );
}